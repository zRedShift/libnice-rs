pub use libc::{sockaddr, sockaddr_in, sockaddr_in6};

#[derive(Copy, Clone)]
#[repr(C)]
pub union NiceAddress_s {
    pub addr: sockaddr,
    pub ip4: sockaddr_in,
    pub ip6: sockaddr_in6,
}

impl std::fmt::Debug for NiceAddress_s {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("NiceAddress_s @ {:p}", self))
            .finish()
    }
}
