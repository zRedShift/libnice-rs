#!/bin/bash
set -x -e

# https://github.com/gtk-rs/gir-files/blob/master/reformat.sh
# `///` used as `//` not works in Windows in this case
for file in *.gir; do
    xmlstarlet ed -L \
        -d '//_:doc/@line' \
        -d '//_:doc/@filename' \
        -d '///_:source-position' \
        "$file"
done

# register nice_address_dup as a copy function
xmlstarlet ed --inplace \
    --update '//_:record[@name="Address"]/_:method[@name="dup"]/@name' \
    --value 'copy' \
    Nice-0.1.gir
