#[allow(unused_imports, clippy::too_many_arguments)]
mod auto;
pub use auto::*;

mod address;
mod agent;
mod candidate;

#[doc(alias = "nice_debug_enable")]
pub fn debug_enable(with_stun: bool) {
    unsafe {
        ffi::nice_debug_enable(with_stun as i32);
    }
}

#[doc(alias = "nice_debug_disable")]
pub fn debug_disable(with_stun: bool) {
    unsafe {
        ffi::nice_debug_disable(with_stun as i32);
    }
}
