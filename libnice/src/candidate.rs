#[cfg(any(feature = "v0_1_18", feature = "dox"))]
#[cfg_attr(feature = "dox", doc(cfg(feature = "v0_1_18")))]
use crate::CandidateTransport;
use crate::{Address, Candidate, CandidateType};
use glib::translate::*;
use libc::c_char;
use std::net::{SocketAddr, SocketAddrV4, SocketAddrV6};
use std::{ffi::CStr, str::Utf8Error};

impl Candidate {
    pub fn type_(&self) -> CandidateType {
        unsafe { CandidateType::from_glib(self.inner.type_) }
    }

    #[cfg(any(feature = "v0_1_18", feature = "dox"))]
    #[cfg_attr(feature = "dox", doc(cfg(feature = "v0_1_18")))]
    pub fn transport(&self) -> CandidateTransport {
        unsafe { CandidateTransport::from_glib(self.inner.transport) }
    }

    #[cfg(any(feature = "v0_1_18", feature = "dox"))]
    #[cfg_attr(feature = "dox", doc(cfg(feature = "v0_1_18")))]
    pub fn set_transport(&mut self, transport: CandidateTransport) {
        self.inner.transport = transport.into_glib();
    }

    pub fn addr(&self) -> SocketAddr {
        unsafe {
            match self.inner.addr.s.addr.sa_family as i32 {
                libc::AF_INET => {
                    let sockaddr_in = self.inner.addr.s.ip4;
                    let ip = sockaddr_in.sin_addr.s_addr.to_ne_bytes().into();
                    let port = u16::from_be(sockaddr_in.sin_port);
                    SocketAddr::V4(SocketAddrV4::new(ip, port))
                }
                libc::AF_INET6 => {
                    let sockaddr_in6 = self.inner.addr.s.ip6;
                    SocketAddr::V6(SocketAddrV6::new(
                        sockaddr_in6.sin6_addr.s6_addr.into(),
                        u16::from_be(sockaddr_in6.sin6_port),
                        sockaddr_in6.sin6_flowinfo,
                        sockaddr_in6.sin6_scope_id,
                    ))
                }
                other => panic!("unsupported address family {other}"),
            }
        }
    }

    pub fn set_addr(&mut self, addr: SocketAddr) {
        let inner_addr = unsafe { &mut *(&mut self.inner.addr as *mut _ as *mut Address) };
        match addr {
            SocketAddr::V4(sock) => {
                inner_addr.set_ipv4(sock.ip());
                inner_addr.set_port(sock.port());
            }
            SocketAddr::V6(sock) => {
                inner_addr.set_ipv6(sock.ip());
                inner_addr.set_port(sock.port());
            }
        }
    }

    pub fn priority(&self) -> u32 {
        self.inner.priority
    }

    pub fn set_priority(&mut self, priority: u32) {
        self.inner.priority = priority;
    }

    pub fn stream_id(&self) -> u32 {
        self.inner.stream_id
    }

    pub fn set_stream_id(&mut self, stream_id: u32) {
        self.inner.stream_id = stream_id;
    }

    pub fn component_id(&self) -> u32 {
        self.inner.component_id
    }

    pub fn set_component_id(&mut self, component_id: u32) {
        self.inner.component_id = component_id;
    }

    pub fn foundation(&self) -> Result<&str, Utf8Error> {
        unsafe { CStr::from_ptr(&self.inner.foundation as *const c_char).to_str() }
    }

    pub fn set_foundation(&mut self, foundation: &str) {
        let mut curr = self.inner.foundation.iter_mut();
        for (new, curr) in foundation.bytes().zip(&mut curr) {
            *curr = new as c_char;
        }
        curr.for_each(|rem| *rem = 0);
    }

    pub fn username(&self) -> Result<Option<&str>, Utf8Error> {
        if self.inner.username.is_null() {
            Ok(None)
        } else {
            unsafe { CStr::from_ptr(self.inner.username).to_str().map(Some) }
        }
    }

    pub fn set_username(&mut self, username: Option<&str>) {
        self.inner.username = username.to_glib_full();
    }

    pub fn password(&self) -> Result<Option<&str>, Utf8Error> {
        if self.inner.password.is_null() {
            Ok(None)
        } else {
            unsafe { CStr::from_ptr(self.inner.password).to_str().map(Some) }
        }
    }

    pub fn set_password(&mut self, password: Option<&str>) {
        self.inner.password = password.to_glib_full();
    }
}
